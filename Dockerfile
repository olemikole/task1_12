FROM gcc:11

RUN apt update --yes
RUN apt install cowsay --yes

ENTRYPOINT [ "/usr/games/cowsay","I am Mark Zuckerberg" ]