# Docker Cowsay
Simple docker image that runs cowsay.


![screenshot](cowsay.png)
## Requirements
´docker´ 

## Usage
```bash
$ docker build https://gitlab.com/olemikole/task1_12.git\#main -t cowsay
$ docer run --rm cowsay
```
## Part 3: Usage of docker
I imagine that docker can be useful when working on an embedded device with not much debugging capabilities. By using contianers you can run the code without having to deploy it on the actual device. Instead you can have a docker image containing the same libraries and software and run the software on the machine from where you are developing.

#### Exploring dockerhub
[Minecraft server](https://hub.docker.com/r/minecraftservers/minecraft-server). This makes it really simple to host a minecraft server using docker.

## Author
[Ole Kjepso (@olemikole)](https://gitlab.com/olemikole)
